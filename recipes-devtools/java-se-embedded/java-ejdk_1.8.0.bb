
SUMMARY = "Oracle Java SE runtime environment binaries"
DESCRIPTION = "This is the proprietary JRE from Sun/Oracle, with the Hotspot JVM. It is\
 provided by Sun/Oracle only in binary format.\
 Java Platform, Standard Edition (Java SE) lets you develop and deploy Java\
 applications on desktops and servers, as well as in today's demanding\
 embedded environments. Java offers the rich user interface, performance,\
 versatility, portability, and security that today's applications require.\
 The JRE (Java Runtime Environment) is needed on a system to run Java\
 applications and applets"


PV_UPDATE = "191"
BUILD_NUMBER = "11"
JVM_VERSION = "compact1"


FILESEXTRAPATHS_append := ":${THISDIR}/download"

LIC_FILES_CHKSUM = "file://${WORKDIR}/ejdk1.8.0_${PV_UPDATE}/linux_armv6_vfp_hflt/compact1/COPYRIGHT;md5=a762796b2a8989b8952b653a178607a1"
LICENSE = "Oracle_Binary_Code_License_Agreement"

SRC_URI = "file://ejdk-8u${PV_UPDATE}-linux-armv6-vfp-hflt.tar.gz"

SRC_URI[md5sum] = "85ceb493211d05ec7cbc8258eaee023e"
SRC_URI[sha256sum] = "5dcc644c999e7109efe46669ac717f3b96f3bbdeac3cb66f072ca4dfbc405b8e"

S = "${WORKDIR}"


FILES_${PN} = "${bindir}/*"

do_install() {
	install -d ${D}${bindir}/ejdk1.8.0
	cp -a ${S}/ejdk1.8.0_${PV_UPDATE}/linux_armv6_vfp_hflt/${JVM_VERSION}/* ${D}${bindir}/ejdk1.8.0/ --no-preserve=ownership

	install -d 0644  ${D}${bindir}
	ln -sfr ${D}${bindir}/ejdk1.8.0/bin/java  ${D}/usr/bin/java
}


INSANE_SKIP_${PN} += "dev-so already-stripped ldflags file-rdeps "

EXCLUDE_FROM_SHLIBS = "1"
