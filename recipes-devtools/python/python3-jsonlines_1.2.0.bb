SUMMARY = "This is a python module simplify working with json lines"
HOMEPAGE = "https://github.com/wbolster/jsonlines"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE.rst;md5=20d7012e64abdf520a406153c397eb59"

SRC_URI[md5sum] = "05f0f16ed7320a6040a9c181623774de"
SRC_URI[sha256sum] = "43b8d5588a9d4862c8a4a49580e38e20ec595aee7ad6fe469b10fb83fbefde88"

PYPI_PACKAGE = "jsonlines"

inherit pypi
inherit setuptools3

BBCLASSEXTEND = "native nativesdk"