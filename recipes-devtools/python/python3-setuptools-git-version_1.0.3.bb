DESCRIPTION = "Plugin for setuptools that enables git integration"
HOMEPAGE = "https://github.com/wichert/setuptools-git"
SECTION = "devel/python"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://README.rst;md5=db37fc18ea101070f23372a046e58f52"

SRC_URI[md5sum] = "802555a15f289a6016cfe10c02a004b7"
SRC_URI[sha256sum] = "82cf7ab80272e20b7cae81d907e0adf295ef7a1f735418c046ae7f40f5861e15"

inherit setuptools3 pypi
PYPI_PACKAGE_EXT="zip"

BBCLASSEXTEND = "native"
