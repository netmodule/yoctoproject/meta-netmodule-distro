SUMMARY = "CppUTest unit testing and mocking framework for C/C++"
HOMEPAGE = "https://cpputest.github.io"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=ce5d5f1fe02bcd1343ced64a06fd4177"

inherit autotools

SRC_URI = "git://github.com/cpputest/cpputest.git;protocol=https"
SRCREV = "bbe3801b990d281cf251cc6e6107256808ea6d19"

PV = "3.8+git${SRCPV}"

S = "${WORKDIR}/git"

# Required to add the libary to the SDK.
# ${PN}-dev depends on ${PN} which is empty because
# only the static library is built
ALLOW_EMPTY_${PN} = "1"

BBCLASSEXTEND = "native nativesdk"
