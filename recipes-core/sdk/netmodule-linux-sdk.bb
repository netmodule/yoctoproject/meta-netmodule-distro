SUMMARY = "NetModule Linux SDK"

LICENSE = "MIT"

inherit populate_sdk

TOOLCHAIN_TARGET_TASK_append = " \
    cpputest-staticdev \
    libnmapp-staticdev \
    libc-staticdev \
"

