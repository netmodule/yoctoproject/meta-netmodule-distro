SUMMARY = "Bootloader management tools for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

PACKAGE_ARCH = "${MACHINE_ARCH}"

RDEPENDS_${PN}-extended = " \
	${@bb.utils.contains("MACHINE_FEATURES", "imx-boot", "", "bootloader-config", d)} \
	"
