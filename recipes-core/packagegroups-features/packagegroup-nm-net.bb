SUMMARY = "Network tools for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

# less is needed for nmcli to show proper output
RDEPENDS_${PN}-minimal = " \
	networkmanager \
	less \
	"

RDEPENDS_${PN}-base = " \
	iproute2 \
	packagegroup-core-ssh-openssh \
	openssh-sftp-server \
	ethtool \
	"

RDEPENDS_${PN}-extended= " \
	iptables \
	iptables-module-ipt-masquerade \
	iptables-module-ip6t-masquerade \
	bridge-utils \
	networkmanager-openvpn \
	"
