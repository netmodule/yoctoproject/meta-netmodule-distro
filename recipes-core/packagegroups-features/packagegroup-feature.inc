inherit packagegroup

PACKAGES = "${PN}-minimal ${PN}-base ${PN}-extended"

SUMMARY_${PN}-minimal = "Minimal packages for ${PN}"
SUMMARY_${PN}-base = "Base packages for ${PN}"
SUMMARY_${PN}-extended = "Extended packages for ${PN}"
