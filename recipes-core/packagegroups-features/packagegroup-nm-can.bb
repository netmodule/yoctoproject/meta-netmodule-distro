SUMMARY = "CAN tools for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

RDEPENDS_${PN}-base = " \
	can-utils \
	"

RDEPENDS_${PN}-extended = " \
	cannelloni \
	"
