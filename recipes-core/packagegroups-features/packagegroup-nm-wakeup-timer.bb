SUMMARY = "Wakeup Timer driver for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

RDEPENDS_${PN}-extended = " \
	wakeup-timer \
	"
