SUMMARY = "SPI tools for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

RDEPENDS_${PN}-base = " \
	spitools \
	"
