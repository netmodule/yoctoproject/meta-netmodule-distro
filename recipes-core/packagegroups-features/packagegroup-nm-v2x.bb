SUMMARY = "V2X tools for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

RDEPENDS_${PN}-extended = " \
	v2x-ieee802.11p \
	"
