SUMMARY = "GNSS tools for NetModule distributions"

LICENSE = "MIT"

require packagegroup-feature.inc

PACKAGE_ARCH = "${MACHINE_ARCH}"

RDEPENDS_${PN}-extended = " \
	gpsd \
	gpsd-conf \
	gps-utils \
	${@bb.utils.contains("MACHINE_FEATURES", "advanced-gnss", "gnss-mgr", "", d)} \
	"
