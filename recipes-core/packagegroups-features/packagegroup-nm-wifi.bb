SUMMARY = "Wifi tools for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

PACKAGE_ARCH = "${MACHINE_ARCH}"

TI_WIFI = " \
	kernel-module-wlan-backports \
	firmware-ti-wl18xx \
	"

ATH_WIFI = " \
	linux-firmware-ath6k \
	linux-firmware-ath9k \
	linux-firmware-ath10k \
	"

# packagegroup-ublox-modules must be configured from machine.conf with UBLOX_FEATURES
# See meta-ublox-modules/recipes-core/packagegroups/packagegroup-ublox-modules.bb
UBLOX_WIFI = " \
	packagegroup-ublox-modules \
	"


RDEPENDS_${PN}-minimal = " \
	${@bb.utils.contains("MACHINE_FEATURES", "ti-wifi", "${TI_WIFI}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "ath-wifi", "${ATH_WIFI}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "ublox-wifi", "${UBLOX_WIFI}", "", d)} \
	"


RDEPENDS_${PN}-extended = " \
	hostapd \
	"
