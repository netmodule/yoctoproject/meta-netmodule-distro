SUMMARY = "Time tools for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

RDEPENDS_${PN}-extended = " \
	util-linux-rtcwake \
	chrony \
	chronyc \
	"
