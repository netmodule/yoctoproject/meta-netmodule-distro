SUMMARY = "FPGA image for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

RDEPENDS_${PN}-base = " \
	fpga-image \
	"
