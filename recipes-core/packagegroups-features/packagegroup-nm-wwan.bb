SUMMARY = "WWAN tools for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

RDEPENDS_${PN}-extended = " \
	modemmanager \
	wwan-config \
	"
