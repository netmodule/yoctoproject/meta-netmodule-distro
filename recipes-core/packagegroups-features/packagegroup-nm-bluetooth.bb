SUMMARY = "Bluetooth tools for NetModule distributions"
LICENSE = "MIT"

require packagegroup-feature.inc

PACKAGE_ARCH = "${MACHINE_ARCH}"

RDEPENDS_${PN}-base = " \
	${@bb.utils.contains("MACHINE_FEATURES", "tibluetooth", "tibluetooth", "", d)} \
	"

RDEPENDS_${PN}-extended = " \
	bluez5-obex \
	bluez5-noinst-tools \
	"
