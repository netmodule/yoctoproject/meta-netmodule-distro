inherit core-image

KERNELDEPMODDEPEND = ""

SUMMARY = "Minimal image for bringup"

NO_RECOMMENDATIONS = "1"

# Minimal NM distribution + tools to fetch and flash images
IMAGE_INSTALL = " \
    packagegroup-nm-minimal \
    e2fsprogs-mke2fs \
    devmem2 \
    curl \
    tar \
    "

LICENSE = "BSD"

BUILDNAME = "${DISTRO_VERSION};${DATETIME};${PN}"

