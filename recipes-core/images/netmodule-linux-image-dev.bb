require netmodule-linux-image.bb

SUMMARY = "netmodule image for developement only"

IMAGE_FEATURES_append = " \
                tools-debug \
                "

MONITORING = " \
                sys-mon \
             "

BENCH_TOOLS = " \
                tcpdump \
                lmbench \
                dbench \
                memtester \
                nbench-byte \
                tiobench \
                iozone3 \
                iperf3 \
                strongswan \
                speedtest-cli \
                benchmarking-cpu \
                stress-ng \
                rt-tests \
                cryptodev-tests \
               "
GPS_TOOLS = " \
              ser2net \
              gnss-mgr-test \
            "


EASY_EDITOR = " \
                nano \
              "

BENCH_TOOLS_cortexa9hf-neon_append = " cpuburn-neon "
BENCH_TOOLS_cortexa8hf-neon_append = " cpuburn-neon "

IMAGE_INSTALL_remove_aarch64 = "benchmarking-cpu"

IMAGE_INSTALL_append = " \
                lrzsz \
                ${BENCH_TOOLS} \
                ${EASY_EDITOR} \
                ${GPS_TOOLS} \
                ${MONITORING} \
                systemd-extra-utils \
                python3 \
                python3-pip \
                python3-pycurl \
                libgpiod-python \
                minicom \
                binutils \
                util-linux \
                i2c-tools \
                libgpiod-tools \
                usbutils \
                pciutils \
                evtest \
                udev-hwdb \
                devmem2 \
                board-descriptor \
                tmate \
                iputils \
                "
OSTREE_BRANCHNAME = "${LAYERSERIES_CORENAMES}-${MACHINE}-dev"
