require netmodule-linux-image-minimal.bb

require includes/image-preprocessing.inc

SUMMARY = "NetModule Linux Distro Image"
PROVIDES += "virtual/netmodule-image"

IMAGE_FEATURES_append = " \
                package-management \
                "

IMAGE_INSTALL = " \
                packagegroup-nm-minimal \
                packagegroup-nm-base \
                packagegroup-nm-extended \
                "


IMAGE_INSTALL_append_sota = " mount-overlay"

LICENSE = "BSD"

IMAGE_OVERHEAD_FACTOR = "1.0"
OSTREE_BOOTLOADER = "u-boot"
OSTREE_BRANCHNAME = "${LAYERSERIES_CORENAMES}-${MACHINE}"

BUILDNAME = "${DISTRO_VERSION};${DATETIME};${PN}"

