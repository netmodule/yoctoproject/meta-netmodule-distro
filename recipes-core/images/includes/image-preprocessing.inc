IMAGE_PREPROCESS_COMMAND_sota += " moveRPMDatabase; dnsmasqlease;"


# Change the location of /var/lib/rpm to /usr/lib/rpm.
# This makes the rpm package index consistent with installed packages.
moveRPMDatabase() {
 cd ${WORKDIR}/rootfs
 mv var/lib/rpm/* usr/lib/rpm
 rm -r var/lib/rpm
 echo "L /var/lib/rpm - - - - /usr/lib/rpm" > etc/tmpfiles.d/ostree-rpm.conf
}

dnsmasqlease() {
 cd ${WORKDIR}/rootfs
 echo "d /var/lib/misc 0755 - - -" > etc/tmpfiles.d/dnsmasq-leasesdir.conf
 cd -
}
