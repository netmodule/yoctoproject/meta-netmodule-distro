inherit packagegroup

PACKAGE_ARCH = "${MACHINE_ARCH}"
# FEATURE_LEVEL must be defined by including file

RDEPENDS_${PN}_append = " \
	\
	packagegroup-nm-net-${FEATURE_LEVEL} \
	packagegroup-nm-boot-tools-${FEATURE_LEVEL} \
	packagegroup-nm-time-${FEATURE_LEVEL} \
	\
	${@bb.utils.contains("MACHINE_FEATURES", "bluetooth", "packagegroup-nm-bluetooth-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "can", "packagegroup-nm-can-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "imu", "packagegroup-nm-imu-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "fpga", "packagegroup-nm-fpga-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "gnss", "packagegroup-nm-gnss-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "spi", "packagegroup-nm-spi-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "user-module", "packagegroup-nm-user-module-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "v2x", "packagegroup-nm-v2x-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "wakeup-timer", "packagegroup-nm-wakeup-timer-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "wifi", "packagegroup-nm-wifi-${FEATURE_LEVEL}", "", d)} \
	${@bb.utils.contains("MACHINE_FEATURES", "wwan", "packagegroup-nm-wwan-${FEATURE_LEVEL}", "", d)} \
	"
