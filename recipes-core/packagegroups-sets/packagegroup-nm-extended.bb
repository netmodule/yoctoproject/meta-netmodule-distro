DESCRIPTION = "Netmodule Linux Extended Packages"
LICENSE = "MIT"

require packagegroup-nm.inc
FEATURE_LEVEL = "extended"

RDEPENDS_${PN} = " \
	packagegroup-nm-base \
	packagegroup-base \
	nmhw-auto-part \
	nmhw-fwupdate \
	${@bb.utils.contains("MACHINE_FEATURES", "da9063-ignition", "ssf-mgr", "", d)} \
	${@bb.utils.contains("DISTRO_FEATURES", "custom-mac-addresses", "mac-address-set", "", d)} \
	storage-info \
	\
	vnstat \
	\
	rng-tools \
	cryptodev-module \
	kernel-modules \
	ca-certificates \
	"

# Hacks for V2X-GNSS-HUB power sequence on HW23
RDEPENDS_${PN}_append_imx8-nmhw23 = " \
	usb-hub-reset \
	gnss-init \
	"
