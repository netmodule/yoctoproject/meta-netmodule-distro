SUMMARY = "Netmodule Linux Base Packages"
LICENSE = "MIT"

require packagegroup-nm.inc
FEATURE_LEVEL = "base"

RDEPENDS_${PN} = " \
	packagegroup-nm-minimal \
	bash \
	curl \
	parted \
	mmc-utils \
	"
