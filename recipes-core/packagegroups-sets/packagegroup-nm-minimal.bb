SUMMARY = "Netmodule Linux Minimal Packages"
LICENSE = "MIT"

require packagegroup-nm.inc
FEATURE_LEVEL = "minimal"

RDEPENDS_${PN} = " \
	packagegroup-core-boot \
	util-linux-agetty \
	systemd-serialgetty \
	ncurses-terminfo-base \
	haveged \
	udev-rules-nmhw \
	"
