DESCRIPTION = "Script that mounts an overlay partition on /usr for debugging purposes"
AUTHOR = "Alexandre Bard"

LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

inherit systemd

SRC_URI =  " \
    file://mount-overlay.service \
    file://mount-overlay.sh \
    "

S = "${WORKDIR}"

INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

SYSTEMD_SERVICE_${PN} = "mount-overlay.service"
SYSTEMD_AUTO_ENABLE ?= "enable"

FILES_${PN}_append = " \
                    /lib \
                    /usr \
                    "

do_install () {
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 mount-overlay.service ${D}${systemd_unitdir}/system/

    install -d ${D}/usr/bin
    install -m 0755 mount-overlay.sh ${D}/usr/bin
}

