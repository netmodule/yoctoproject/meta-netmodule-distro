#!/bin/sh
chattr -i / ;
[[ -e /etc/nmhw-auto-part/data-partition ]] && mkdir -p data && mount $(cat /etc/nmhw-auto-part/data-partition) /data && \
[[ -e /etc/nmhw-auto-part/overlay ]] && mount -t overlay -o lowerdir=/usr,upperdir=/data/overlay,workdir=/data/.work overlay /usr
exit 0
