#!/bin/bash
ACC_SYSFS_PATH="/sys/bus/iio/devices/iio:device0"
GYRO_SYSFS_PATH="/sys/bus/iio/devices/iio:device1"

if [ -d "$ACC_SYSFS_PATH/buffer" ]; then
	buffer_enable=$(cat "$ACC_SYSFS_PATH/buffer/enable")
	if [ "$buffer_enable" = "1" ]; then
		echo 0 > "$ACC_SYSFS_PATH/buffer/enable"
	fi
	echo "$ACC_SAMPLING_FREQUENCY" > $ACC_SYSFS_PATH/sampling_frequency
	if [ "$buffer_enable" = "1" ]; then
		echo 1 > "$ACC_SYSFS_PATH/buffer/enable"
	fi


	buffer_enable=$(cat $GYRO_SYSFS_PATH/buffer/enable)
	if [ "$buffer_enable" = "1" ]; then
		echo 0 > "$GYRO_SYSFS_PATH/buffer/enable"
	fi
	echo "$GYRO_SAMPLING_FREQUENCY" > $GYRO_SYSFS_PATH/sampling_frequency
	if [ "$buffer_enable" = "1" ]; then
		echo 1 > "$GYRO_SYSFS_PATH/buffer/enable"
	fi
else
	imu-poll &
fi
exit 0
