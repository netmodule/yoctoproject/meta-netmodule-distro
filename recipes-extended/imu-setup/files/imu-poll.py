#!/usr/bin/env python3

import os
import time

def create_pty(dev):
    master, slave = os.openpty()
    slave_file = os.readlink('/proc/self/fd/{}'.format(slave))
    if os.path.lexists(dev):
         os.remove(dev)
    os.symlink(slave_file, dev)
    os.close(slave)
    return master


def set_sampling_freq(iio_nbr):
    f = open('/sys/bus/iio/devices/iio:device{}/sampling_frequency'.format(iio_nbr), 'w')
    f.write('416')

set_sampling_freq(1)
set_sampling_freq(2)

dev = create_pty('/dev/imu0')

while True:
    os.write(dev, b'poll\n')
    time.sleep(0.1)
