# Copyright (C) 2019 Lucien Mueller <lucien.mueller@netmodule.com>
# Released under the MIT license (see COPYING.MIT for the terms)
inherit systemd
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
FILESEXTRAPATHS_prepend := "${THISDIR}/config:"

RDEPENDS_${PN} = "bash"

SRC_URI = " \
            file://imu-setup.service \
            file://imu-setup.sh \
            file://imu-poll.py \
            file://imu-lsm6ds3.conf \
          "

FILES_${PN} = "${systemd_unitdir}/system ${bindir} /etc"

SYSTEMD_SERVICE_${PN} =" \
                            imu-setup.service \
                        "

do_install() {

    install -d ${D}${systemd_unitdir}/system
    install -m 644 ${WORKDIR}/imu-setup.service ${D}${systemd_unitdir}/system/

    install -d ${D}${bindir}
    install -m 744 ${WORKDIR}/imu-setup.sh ${D}${bindir}/imu-setup
    install -m 755 ${WORKDIR}/imu-poll.py ${D}${bindir}/imu-poll

    install -d ${D}/etc
    install -m 644 ${WORKDIR}/imu-lsm6ds3.conf ${D}/etc/imu-lsm6ds3.conf
}

