SUMMARY = "NetModule system monitoring app"
SECTION = "base"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=bde2a6343527f4ace81c3fb4ca0d2742"

SRC_URI = "git://gitlab.com/netmodule/tools/sys-mon.git;protocol=ssh;user=git;branch=develop"
SRCREV ?= "dbdbc1e31816239586b4706d962a16feb77bd1cf"

PV = "0.0.1+git${SRCPV}"


DEPENDS_append = " libnmapp"

inherit systemd

S = "${WORKDIR}/git"
B = "${S}/build"

SYSTEMD_SERVICE_${PN} = "sys-mon.service"

FILES_${PN} += " \
                  ${bindir}/sys-mon \
                  ${systemd_system_unitdir}/sys-mon.service \
                  ${sysconfdir}/sys-mon/sys-mon.conf \
               "

# build variables for the target (rest is default)
EXTRA_OEMAKE_append = " 'BUILD_TARGET=target' 'BUILD_CONFIG=rls' 'SYSROOT=${STAGING_DIR_TARGET}'"

do_install() {
    install -d ${D}${bindir}
    install -m 755 ${B}/rls/target/${BPN} ${D}${bindir}
    install -d ${D}${sysconfdir}/sys-mon
    install -m 644 ${S}/config/${BPN}.conf ${D}${sysconfdir}/sys-mon/
    install -d ${D}${systemd_system_unitdir}
    install -m 644 ${S}/systemd/${BPN}.service ${D}${systemd_system_unitdir}
}

