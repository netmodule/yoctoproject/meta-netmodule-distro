SUMMARY = "Generic NetModule Update tool"
DESCRIPTION = "Generic NetModule update tool for firmware, bootloaders, etc"
AUTHOR = "Alexandre Bard"

SECTION = "core"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

RDEPENDS_${PN} = " \
              wget \
              bash \
              coreutils \
              expect \
                "

SRC_URI =  " \
    git://gitlab.com/netmodule/tools/nmhw-fwupdate.git;user=git;protocol=ssh; \
    "
SRCREV ?= "68b84ce457e8cd4a59e2f366114edf0098acbe4d"

S = "${WORKDIR}/git/src"

FILES_${PN} = " \
    /lib \
    /usr \
    "

inherit allarch

do_install () {
    install -d ${D}/usr/bin
    install -m 0755 nmhw-fwupdate.sh ${D}/usr/bin/nmhw-fwupdate
    install -m 0755 list-devices.sh ${D}/usr/bin/list-devices

    install -d ${D}/usr/lib
    install -m 0755 lib-nmhw/* ${D}/usr/lib/
}
