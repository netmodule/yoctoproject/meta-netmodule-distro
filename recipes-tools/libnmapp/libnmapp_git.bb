SUMMARY = "NetModule shared library"
SECTION = "base"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1f002990697cc3f88357d1c0790d47b2"

SRC_URI = "git://gitlab.com/netmodule/tools/libnmapp.git;protocol=ssh;user=git;branch=develop"
SRCREV ?= "dcf72e619b3a5085bba14db42e3a0d86cb4acdc8"

PV = "0.0.1+git${SRCPV}"
LIB_SHORT_NAME = "nmapp"
LIB_PKG_NAME = "libnmapp.a"

DEPENDS_append = " libevent systemd"

# Required to add the libary to the SDK.
# ${PN}-dev depends on ${PN} which is empty because
# only the static library is built
ALLOW_EMPTY_${PN} = "1"

S = "${WORKDIR}/git"
B = "${S}/build"

# build variables for the target (rest is default)
EXTRA_OEMAKE_append = " 'BUILD_TARGET=target' 'BUILD_CONFIG=rls' 'USE_INITSYS_DUMMY=0' 'SYSROOT=${STAGING_DIR_TARGET}'"

do_install() {
    install -d ${D}${libdir}
    install -m 644 ${B}/rls/target/${LIB_PKG_NAME} ${D}${libdir}

    install -d ${D}${includedir}/${LIB_SHORT_NAME}
    cp -r ${S}/include/${LIB_SHORT_NAME}/* ${D}${includedir}/${LIB_SHORT_NAME}
}

BBCLASSEXTEND = "native nativesdk"
