SUMMARY = "Adds a script and a systemd service to partition the drives of the system on the first boot."
DESCRIPTION = "Adds a script and a systemd service to partition the drives of the system on the first boot."
PR = "r1"
LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://COPYING;md5=2f31b266d3440dd7ee50f92cf67d8e6c"


RDEPENDS_${PN} += "e2fsprogs-resize2fs \
                   e2fsprogs-mke2fs \
                   bash \
                   parted \
                  "

SRC_URI = "file://nmhw-auto-part \
           file://COPYING \
          "


S = "${WORKDIR}"

do_install () {
  install -d -m 755 ${D}${bindir}/
  install -m 755 nmhw-auto-part ${D}${bindir}/
}

FILES_${PN} = " \
                    ${bindir}/nmhw-auto-part \
                  "
