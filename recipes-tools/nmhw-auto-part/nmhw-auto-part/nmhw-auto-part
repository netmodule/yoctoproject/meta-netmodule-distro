#!/bin/bash
# This scripts expands the first partition on a block device and appends an additional partition to the device.

green="\e[32m"
normal="\e[0m"
bold="\e[1m"

detect_mmc(){

    local rootcmd
    local bootpart

    if [ -f /proc/cmdline ] ; then

        rootcmd=$(cat /proc/cmdline | sed "s/ /\n/g" | grep "root=/dev/m")
        if [[ $rootcmd == *"mmc"* ]]; then

                if [[ $rootcmd == *"ostree_root"* ]]; then
                    bootpart=${rootcmd//ostree_root=/}

                elif [[ $rootcmd == *"root"* ]]; then
                    bootpart=${rootcmd//root=/}
                fi

            #echo Device: ${bootpart:11:1}
            #echo Partnbr: ${bootpart:13:1}
            mmc_detect_part="/dev/mmcblk${bootpart:11:1}p${bootpart:13:1}"

                if [[ $mmc_detect_part != $bootpart ]]; then
                    echo Wrong mmc detected
                    return 1
                else
                    mmc_detect_dev="/dev/mmcblk${bootpart:11:1}"
                    #echo MMC $mmc_detect_dev found
                fi

        else
            echo No mmc boot
            return 1
        fi
    fi
}

device_available(){
  local device=$1
  if [ -e $device ] ; then
    local partdevice_first
    partdevice_first=$(ls $device*[1-9] | tail -n 1)
    local partn_first
    partn_first=${partdevice_first: -1}
    if (( $partn_first > 1 )) ; then
      echo "Device $device has already too many partitions."
      echo "It seems like $device is already partitioned."
      echo "Please use this script only once."
      exit 1
    else
      return 0
    fi
  else
    echo "Device $device not available."
    return 1
  fi
}

rootsize_ok(){
  local devicename=$1
  local partname=$2
  local newsize=$3
  local size
  size=$(( $(cat /sys/block/$devicename/$partname/size) * $(cat /sys/block/$devicename/queue/hw_sector_size) / 1024 / 1024 ))

  if [ $newsize -eq 0 ] ; then
    return 0
  fi
  if [ "$newsize" -gt "$size" ]; then
    return 0
  else
    echo "Error: The root partition is already bigger than $newsize MB!"
    echo "Select a larger size than $size MB or enter 0 to keep the current size."
    return 1
  fi
}

type_available(){
  local ptype=$1
  case "$ptype" in
  "regular")
      return 0
      ;;
  "overlay")
      return 0
      ;;
  *)
      echo "Error: $ptype is not an option."
      return 1
      ;;
  esac
}

# Arguments
# 1 - device e.g.  /dev/mmcblk1
# 2 - partition number
# 3 - new size
expand_partition(){
  local device=$1
  local partn=$2
  local newsize=$3

  if [ $newsize -eq 0 ] ; then
    return 0
  fi

  # Get the partiton device e.g. /dev/mmcblk1p2
  local partdevice
  partdevice=$(ls $device*[$partn] | tail -n 1)

  # get the name e.g. mmcblk1
  local devicename
  devicename=$(echo $device | cut -d '/' -f 3)

  # get the name of the otaroot partition e.g. mmcblk1p2
  local partname
  partname=$(echo $partdevice | cut -d '/' -f 3)

  # first sector
  local start
  start=$(cat /sys/block/$devicename/$partname/start)

  # last sector
  local end
  end=$(($start+$(cat /sys/block/$devicename/$partname/size)))

  # calculate new last sector
  local newend
  newend=$(($start + $newsize * 1024 * 1024 / $(cat /sys/block/$devicename/queue/hw_sector_size)))

  parted $device ---pretend-input-tty resizepart 1 <<EOF >> /tmp/log/nmhw-auto-part.log 2>&1 || true
Yes
${newend}s
EOF

  partprobe $device >> /tmp/log/nmhw-auto-part.log 2>&1 || true
  resize2fs $partdevice >> /tmp/log/nmhw-auto-part.log 2>&1
}


# Arguments
# 1 - device e.g.  /dev/mmcblk1
# 2 - size in MB (if size is 0, the partition will fill the whole device)
# returns createdPartition
append_partition(){
  local device=$1
  local size=$2

  # Get the previous partiton device e.g. /dev/mmcblk1p2
  local partdevice_prev
  partdevice_prev=$(ls $device*[1-9] | tail -n 1)
  local partn_prev
  partn_prev=${partdevice_prev: -1}

  if [ $partn_prev -eq 4 ] ; then
    echo "Error:You can only have a maximum of 4 partitions."
    return 1
  fi

  local partn
  partn=$(($partn_prev + 1))

  # get the name e.g. mmcblk1
  local devicename
  devicename=$(echo $device | cut -d '/' -f 3)

  # get the name of the previous partition e.g. mmcblk1p2
  local partname_prev
  partname_prev=$(echo $partdevice_prev | cut -d '/' -f 3)

  # first sector
  local start_prev
  start_prev=$(cat /sys/block/$devicename/$partname_prev/start)

  # last sector of previous partition
  local end_prev
  end_prev=$(($start_prev+$(cat /sys/block/$devicename/$partname_prev/size)))

  local start
  start=$(($end_prev + 1))

  local end_device
  end_device=$(($(cat /sys/block/$devicename/size)-8))

  if [ $size -eq 0 ] ; then
    local end=$end_device
  else
    local end=$(($start + $size * 1024 * 1024 / $(cat /sys/block/$devicename/queue/hw_sector_size)))
    if [ $end -gt $end_device ] ; then
      end=$end_device
    fi
  fi

  parted -s $device mkpart primary ext4 ${start}s ${end}s >> /tmp/log/nmhw-auto-part.log 2>&1 || true
  partprobe $device >> /tmp/log/nmhw-auto-part.log 2>&1 || true

  # Get the partiton device e.g. /dev/mmcblk1p2
  local partdevice
  partdevice=$(ls $device*[$partn] | tail -n 1)
  mkfs.ext4 $partdevice >> /tmp/log/nmhw-auto-part.log 2>&1

  createdPartition=$partdevice
}

interactive_mode(){

  printf "| You are now in interactive mode.\n"
  printf "| Please enter the following values.\n"
  printf "|$bold It's advised, that you use the recommended values in the$green [square brackets]$normal.\n"
  printf "|$bold Press enter to automatically apply the recommended values.$normal\n"

  d=0
  while [ $d -eq 0 ]; do
    detect_mmc
    printf "Enter device to expand $bold$green[$mmc_detect_dev]$normal:\n"
    read device
    if [ -z ${device} ] ; then
      device=$mmc_detect_dev
      printf "Using $device\n\n"
    fi
    device_available $device && d=1
  done

  # number of the otaroot partiton
  otarootpartn=1

  # Get the otaroot partiton device e.g. /dev/mmcblk1p2
  otarootpart=$(ls $device*[$otarootpartn] | tail -n 1)

  # get the name e.g. mmcblk1
  devicename=$(echo $device | cut -d '/' -f 3)

  # get the name of the otaroot partition e.g. mmcblk1p2
  otarootpartname=$(echo $otarootpart | cut -d '/' -f 3)

  d=0
  while [ $d -eq 0 ]; do
    printf "Enter size of the first partition in MB. 0 = keep current size. $bold$green[1024]$normal:\n"
    read newrootsize
    if [ -z ${newrootsize} ] ; then
      newrootsize="1024"
      printf "Using $newrootsize\n\n"
    fi
    rootsize_ok $devicename $otarootpartname $newrootsize && d=1
  done

  d=0
  while [ $d -eq 0 ] ; do
    printf "Enter type of partition to append ('regular' or 'overlay') $bold$green[overlay]$normal:\n"
    read ptype
    if [ -z ${ptype} ] ; then
      ptype="overlay"
      printf "Using $ptype\n\n"
    fi
    type_available $ptype && d=1
  done

  printf "Enter size of the partition to append in MB. 0 = use remaining space. $bold$green[0]$normal:\n"
  read datasize
  if [ -z ${datasize} ] ; then
    datasize="0"
    printf "Using $datasize\n\n"
  fi
  execute
}

scripted_mode(){

  device=$1
  newrootsize=$2
  ptype=$3
  datasize=$4

  if ! device_available $device ; then
    printf "Device $device "
    exit 1
  fi

  # number of the otaroot partiton
  otarootpartn=1

  # Get the otaroot partiton device e.g. /dev/mmcblk1p2
  otarootpart=$(ls $device*[$otarootpartn] | tail -n 1)

  # get the name e.g. mmcblk1
  devicename=$(echo $device | cut -d '/' -f 3)

  # get the name of the otaroot partition e.g. mmcblk1p2
  otarootpartname=$(echo $otarootpart | cut -d '/' -f 3)

  if ! rootsize_ok $devicename $otarootpartname $newrootsize ; then
    exit 1
  fi

  if ! type_available $ptype ; then
    exit 1
  fi
  execute

}

execute(){
  printf "Expanding partition $otarootpartn of device $device. "
  expand_partition $device $otarootpartn $newrootsize
  printf "Done.\n\n"

  printf "Appending partition with size $datasize on device $device. "
  append_partition $device $datasize
  printf "Done.\n\n"
  datapart=$createdPartition

  printf "Mounting new partition to /data. "
  mkdir -p /data
  mount $datapart /data
  printf "Done.\n\n"

  mkdir -p /etc/nmhw-auto-part
  echo $datapart > /etc/nmhw-auto-part/data-partition

  case "$ptype" in
  "overlay")

      mkdir -p /data/.work
      printf "Creating overlay folder in /data. "
      mkdir -p /data/overlay
      printf "Done.\n\n"

      printf "Mounting overlay. "
      mount -t overlay -o lowerdir=/usr,upperdir=/data/overlay,workdir=/data/.work overlay /usr
      printf "Done.\n\n"

      touch /etc/nmhw-auto-part/overlay
      ;;
  "regular")

      ;;
  *)
      echo "$ptype is not an option."
      ;;
  esac

  echo "Partitioning successful."
  echo "Partitioning successful." >> /tmp/log/nmhw-auto-part.log

  echo "A new partition is now available and mounted on /data."
  if [ "$ptype" == "overlay" ] ; then
    echo "There is now an overlay mounted over /usr."
    echo "/usr is now writeable."
    echo "All changes to /usr are now saved under /data/overlay."
  fi
}

usage(){
  printf "Usage: nmhw-auto-part [-h|-i] \n"
  printf "       nmhw-auto-part disk size1 mode size2 \n"
}

description(){
  printf "\
This scripts expands the first partition on a block device and appends an additional partition to the device. \n\
Arguments: \n\
disk       : disk to use: e.g. /dev/mmcblk1 \n\
size1      : size of the first partition in MB. 0 = keep current size. \n\
mode       : regular or overlay; type of partition to append \n\
size2      : size of the partition to append in MB. 0 = use remaining space. \n\
-i         : interactive mode\n\
-h         : help\n"
}

# exit when any commands fails
set -e

mkdir -p /tmp/log
echo "Script started $(date)." >> /tmp/log/nmhw-auto-part.log
echo "Script run with parameters 1: $1 2: $2 3: $3 4: $4 5: $5" >> /tmp/log/nmhw-auto-part.log
echo "" >> /tmp/log/nmhw-auto-part.log
echo "" >> /tmp/log/nmhw-auto-part.log

if (( $# == 0 )) ; then
  usage
  description
  exit 0
elif (( $# == 1 )) ; then
  case $1 in
  "-i")
      interactive_mode
      exit 0
      ;;
  "-h")
      usage
      description
      exit 0
      ;;
    *)
      echo "${0##*/} : $1 is not an option."
      usage
      description
      exit 1
      ;;
  esac
elif (( $# == 4 )) ; then
      scripted_mode $1 $2 $3 $4
      exit 0
else
    echo "${0##*/} : Illegal number of parameters."
    usage
    description
    exit 1
fi
