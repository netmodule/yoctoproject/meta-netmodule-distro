SUMMARY = "Battery Test tool"
DESCRIPTION = "Read battery state, remaining time and other informations from battery"
AUTHOR = "Ramon Mösching (ramon.moesching@netmodule.com)"
SECTION = "core"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
PR = "r0"

RDEPENDS_${PN} = "python3-core python3-smbus"

SRC_URI =  "git://git.netmodule.intranet/nm-tools/smart-battery.git;protocol=ssh;user=gitea;branch=master "

SRCREV ?= "7caae36f50ea23f69458fc50887c5ab59b4d48f3"

S = "${WORKDIR}/git"

FILES_${PN} = "${bindir}/* batterytest.py  "


do_install () {
	install -d ${D}${bindir}
	install -m 0755 batterytest.py ${D}${bindir}/batterytest

}

