SUMMARY = "NetModule system-state-framework manager"
SECTION = "base"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://LICENSE;md5=6f1c528c9a0010ef398e26c661ff286e"

SRC_URI = "git://gitlab.com/netmodule/tools/ssf-mgr.git;protocol=ssh;user=git;branch=develop"
SRCREV ?= "eead025f7135b28cb6e3e1e68bd636e93b74798b"

PV = "0.0.1+git${SRCPV}"


DEPENDS_append = " libnmapp"
RDEPENDS_${PN} = " packagegroup-system-state-framework"

inherit systemd

S = "${WORKDIR}/git"
B = "${S}/build"

SYSTEMD_SERVICE_${PN} = "ssf-mgr.service"

FILES_${PN} += " \
                  ${bindir}/ssf-mgr \
                  ${systemd_system_unitdir}/ssf-mgr.service \
                  ${sysconfdir}/ssf-mgr/ssf-mgr.conf \
               "

# build variables for the target (rest is default)
EXTRA_OEMAKE_append = " 'BUILD_TARGET=target' 'BUILD_CONFIG=rls' 'SYSROOT=${STAGING_DIR_TARGET}'"

do_install() {
    install -d ${D}${bindir}
    install -m 755 ${B}/rls/target/${BPN} ${D}${bindir}
    install -d ${D}${sysconfdir}/ssf-mgr
    install -m 644 ${S}/config/${BPN}.conf ${D}${sysconfdir}/ssf-mgr/
    install -d ${D}${systemd_system_unitdir}
    install -m 644 ${S}/systemd/${BPN}.service ${D}${systemd_system_unitdir}
}

