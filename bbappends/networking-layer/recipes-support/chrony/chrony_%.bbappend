# Copyright (C) 2020 Ramon Moesching <ramon.moesching@netmodule.com>
# Released under the MIT license (see COPYING.MIT for the terms)
FILESEXTRAPATHS_prepend:= "${THISDIR}/files:"

do_install_append () {
	if ${@bb.utils.contains("MACHINE_FEATURES", "pps", "true", "false", d)}; then
		sed -i '/^#refclock PPS.*/s/^#//' ${D}/${sysconfdir}/chrony.conf
	fi
}

# chrony needs continuously running nmea stream and for
# thus gpsd daemon needs to start with -n paramteter.
# Patching at installation time gpsd configuration file
# to apply this parameter
pkg_postinst_${PN}_prepend () {

	GPSD_CONF=$D${sysconfdir}/default/gpsd.default

	if [ -e "$GPSD_CONF" ]; then
		GPSD_OPS=$(awk /GPSD_OPTIONS/ "$GPSD_CONF" )
		GPSD_OPSPARAM=$(echo "$GPSD_OPS" | awk /-n/)
		if [ -z  "$GPSD_OPSPARAM" ]; then
			GPSD_OPSPARAM="${GPSD_OPS%\"} -n\""
			sed  -i "s/$GPSD_OPS/$GPSD_OPSPARAM/g" "$GPSD_CONF"
			echo "Patched $GPSD_CONF with -n"
		fi
		echo "Apply -n to GPSD_OPTIONS to use chrony "
	else
		echo "GPSD configuration file not at $GPSD_CONF "
	fi

}
