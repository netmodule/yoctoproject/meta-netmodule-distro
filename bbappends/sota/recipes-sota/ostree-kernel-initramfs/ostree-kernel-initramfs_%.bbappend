# The main recipe is expecting the fitimage to be built direclty
# by virtual/kernel. But in our case virtual/kernel is building a
# zImage/Image and we pack the fitimage in netmodule-fitimage

do_install[depends] += " netmodule-fitimage:do_deploy"
KERNEL_IMAGETYPE = "fitImage"
