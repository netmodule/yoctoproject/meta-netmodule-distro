FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

do_install_append() {
    # Remove default network configurations
    rm -rf ${D}${systemd_unitdir}/network
}
