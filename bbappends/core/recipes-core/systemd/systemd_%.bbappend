PACKAGECONFIG_remove = "nss-resolve resolved"
ALTERNATIVE_${PN} = "halt reboot shutdown poweroff runlevel ${@bb.utils.contains('PACKAGECONFIG', 'resolved', 'resolv-conf', '', d)}"
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI_append = " \
                 file://journald.conf \
                 "

# The only reason for this line is to remove os-release from "dependencies" of systemd
# Having this dependency leads to a rebuild of systemd when os-release is rebuilt,
# even when the recommendations are excluded.
# It may not be required with future releases of yocto.
RRECOMMENDS_${PN}_remove = "os-release"

do_install_append() {
    # Remove default network configurations
    rm -rf ${D}${systemd_unitdir}/network

    # bring our default config file in
    install -d ${D}${sysconfdir}/systemd
    install -m 644 ${WORKDIR}/journald.conf ${D}${sysconfdir}/systemd
}
