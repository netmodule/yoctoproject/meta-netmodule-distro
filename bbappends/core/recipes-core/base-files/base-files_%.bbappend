BASEFILESISSUEINSTALL =  "do_install_basefilesissue"

DISTROVERSION-ISSUE = "\S{PRETTY_NAME}\n"
DISTROVERSION-ISSUE_append= "kernel \\\r\n"
DISTROVERSION-ISSUE_append = "\\\n @ \\\l\n"

#r: kernel version
#n: hostname
#l: terminal device



do_install_basefilesissue() {

	if [ "${hostname}" ]; then
		echo ${hostname} > ${D}${sysconfdir}/hostname
	fi

	install -m 644 ${WORKDIR}/issue*  ${D}${sysconfdir}
	printf "${DISTROVERSION-ISSUE}\n" >> ${D}${sysconfdir}/issue
	printf "${DISTROVERSION-ISSUE}" >> ${D}${sysconfdir}/issue.net
	echo -e "%h\n" >> ${D}${sysconfdir}/issue.net
}
