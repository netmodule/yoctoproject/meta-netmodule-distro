#!/usr/bin/env bash

UM_CONFIG_PATH=/etc/user-module
UM_CONFIG_FILE=network.conf
UM_CONFIG=$UM_CONFIG_PATH/$UM_CONFIG_FILE
UM_PORTS_V1=$UM_CONFIG_PATH/ports-v1
UM_PORTS_V2=$UM_CONFIG_PATH/ports-v2

UM_DTS_NODE=/proc/device-tree/user_module


if [ -f $UM_CONFIG  ]; then
    rm $UM_CONFIG
else
    mkdir -p $UM_CONFIG_PATH
fi

STATE="$(tr -d '\0' < "$UM_DTS_NODE/status" )"

if [ "$STATE" = "disabled" ]
then
   echo UM is disabled. Exit now.
   exit 0
elif [ "$STATE" = "okay" ]
then
   echo UM detected. Store UM information into $UM_CONFIG
else
   echo UM status not available. Exit now.
   exit 0
fi


# fetch user module status and network informations
for f in $UM_DTS_NODE/*; do
    echo "USER_MODULE_${f##*/}=\"`cat $f | tr -d '\0' `\"" | sed -E "s/,|-/_/g"  >> $UM_CONFIG
done

source $UM_CONFIG

max_retry=15
counter=0

until [[ counter -ge $max_retry ]]
do
  echo "Probing um. #$counter"
  ping -qc 1 -W 1 $USER_MODULE_ipv4_addr && break
  sleep 1
  ((counter++))
done

if ! ping -qc 1 -W 1 $USER_MODULE_ipv4_addr
then
    echo "UM not reachable"
    exit 1
fi

# Decide which config to load depending on the sw revision
if `nc -z $USER_MODULE_ipv4_addr 7000` ; then
  echo "New UM software detected"
  cat $UM_PORTS_V2 >> $UM_CONFIG
else
  echo "Old UM software detected"
  cat $UM_PORTS_V1 >> $UM_CONFIG
fi

exit 0
