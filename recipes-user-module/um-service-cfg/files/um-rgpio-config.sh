#!/bin/sh
UM_SYSFS_REMOTE_GPIO="/sys/class/remote-gpio/remote-gpio/config"

if [ -z $USER_MODULE_ipv4_addr  ]; then
    echo Start script by systemd service or set variables
else
    if [ -d $UM_SYSFS_REMOTE_GPIO ]; then
        if [[ `cat $UM_SYSFS_REMOTE_GPIO/ip` !=  $USER_MODULE_ipv4_addr || `cat $UM_SYSFS_REMOTE_GPIO/port` !=  $USER_MODULE_ipv4_port ]]; then
            echo $USER_MODULE_ipv4_addr > $UM_SYSFS_REMOTE_GPIO/ip
            echo $USER_MODULE_remote_gpio_port > $UM_SYSFS_REMOTE_GPIO/port
        fi
        exit 0
    fi
fi
exit 1
