# Copyright (C) 2019 Ramon Moesching <ramon.moesching@netmodule.com>
# Released under the MIT license (see COPYING.MIT for the terms)
inherit systemd
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
FILESEXTRAPATHS_prepend := "${THISDIR}/config:"

ALTERNATIVE_PRIORITY[netcat] = "100"
RDEPENDS_${PN} = "netcat"

SRC_URI = " \
            file://um-service-config.service \
            file://um-service-config.sh \
            file://user-module.target \
            file://um-rgpio-config.sh \
            file://um-rgpio-config.service \
            file://ports-v1 \
            file://ports-v2 \
          "

FILES_${PN} = "${systemd_unitdir}/system ${bindir} /etc/user-module"

SYSTEMD_SERVICE_${PN} =" \
                            um-service-config.service \
                            um-rgpio-config.service \
                            user-module.target \
                        "

do_install() {

    install -d ${D}${systemd_unitdir}/system
    install -m 644 ${WORKDIR}/um-service-config.service ${D}${systemd_unitdir}/system/

    install -d ${D}${systemd_system_unitdir}/
    install -m 0644 ${WORKDIR}/user-module.target ${D}${systemd_system_unitdir}/

    install -d ${D}${systemd_system_unitdir}/multi-user.target.wants
    ln -sf ${systemd_system_unitdir}/user-module.target ${D}${systemd_system_unitdir}/multi-user.target.wants/user-module.target

    install -d ${D}${bindir}
    install -m 744 ${WORKDIR}/um-service-config.sh ${D}${bindir}/um-service-config

    install -d ${D}${systemd_unitdir}/system
    install -m 644 ${WORKDIR}/um-rgpio-config.service ${D}${systemd_unitdir}/system/
    install -d ${D}${bindir}
    install -m 744 ${WORKDIR}/um-rgpio-config.sh ${D}${bindir}/um-rgpio-config

    install -d ${D}/etc/user-module
    install -m 644 ${WORKDIR}/ports-v1 ${D}/etc/user-module/ports-v1
    install -m 644 ${WORKDIR}/ports-v2 ${D}/etc/user-module/ports-v2
}

