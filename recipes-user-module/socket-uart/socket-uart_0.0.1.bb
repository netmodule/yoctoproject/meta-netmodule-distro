inherit systemd
SUMMARY = "Bind socket-uart with speudo tty based on socat"
DESCRIPTION = "Bind to origin uart which is provided as socket-uart as pseudo tty based on socat"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
                    "
SRC_URI = " \
           file://socket-uart.service \
           file://socat-socket-uart \
           file://socket-kline.service \
           file://socat-socket-kline \
           file://socket-lin.service \
           file://socat-socket-lin \
          "

S = "${WORKDIR}"

RDEPENDS_${PN} = "socat um-service-cfg"


SYSTEMD_SERVICE_${PN} = " \
                              socket-uart.service \
                              socket-kline.service \
                              socket-lin.service \
                              "

FILES_${PN} = "${systemd_unitdir}/system ${bindir} "

do_install() {
    install -d ${D}${systemd_unitdir}/system
    install -m 644 ${WORKDIR}/socket-uart.service ${D}${systemd_unitdir}/system/
    install -m 644 ${WORKDIR}/socket-kline.service ${D}${systemd_unitdir}/system/
    install -m 644 ${WORKDIR}/socket-lin.service ${D}${systemd_unitdir}/system/


    install -d ${D}${bindir}
    install -m 744 ${WORKDIR}/socat-socket-uart ${D}${bindir}
    install -m 744 ${WORKDIR}/socat-socket-kline ${D}${bindir}
    install -m 744 ${WORKDIR}/socat-socket-lin ${D}${bindir}
}
