DESCRIPTION = "CPU Benchmarking bin"
HOMEPAGE = "http://www.netmodule.com/"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

PROVIDES = "benchmarking-cpu"

PV = "1.0.0"

SRC_URI = " \
           file://dhry \
           file://coremark \
           file://memspeed \
           "

FILES_${PN} += "/usr/bin/*"

S = "${WORKDIR}"

do_install () {
    install -d ${D}/${bindir}
    install -m 0755 ${S}/dhry     ${D}/${bindir}
    install -m 0755 ${S}/coremark ${D}/${bindir}
    install -m 0755 ${S}/memspeed ${D}/${bindir}
}
